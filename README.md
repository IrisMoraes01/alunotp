
# AlunoTP

a) Crie seu próprio repositório no GitLab denominado AlunoTP;
b) Crie uma pasta chamada projeto e exercícios e na pasta exercícios inclua a resolução dos exercícios GIT;
c) Envie o commit para o repositório remoto;
d) Adicione um arquivo denominado README.md ao projeto e inclua o seguinte conteúdo “Repositório de exercícios das aulas de Técnicas de Programação”;
e) Adicione uma nova pasta dentro da pasta exercícios chamado exercicio_java. Busque um exercício que você já realizou e suba para o repositório;
f) Crie também um arquivo chamado README.md na raiz da pasta e inclua o cabeçalho deste exercício;
