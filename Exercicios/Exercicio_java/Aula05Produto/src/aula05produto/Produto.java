/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula05produto;

import javax.swing.JOptionPane;

public class Produto {
    String marca, fabricante, cod_produto;
    double preco;

    public Produto(String m, String f, String cod_prod, double p) {
        m = marca;
        f = fabricante;
        cod_prod = cod_produto;
        p = preco;
    }

    public Produto() {
    }
    void CadastraProduto(){
        marca = JOptionPane.showInputDialog(null,"Digite a marca do produto: ");
        fabricante = JOptionPane.showInputDialog(null, "Digite o fabricante do produto: ");
        cod_produto = JOptionPane.showInputDialog(null,"Digite o código do produto: ");
        preco = Double.parseDouble(JOptionPane.showInputDialog("Digite o preço do produto: "));
    }
    void ImprimeProduto(){
        JOptionPane.showMessageDialog(null, "Marca: "+marca+"\n"
                + "Fabricante: "+fabricante+"\nCódigo do produto: "+cod_produto+"\n"
                        + "Preço: "+preco);
    }
    void exibeCodProduto(){
        JOptionPane.showMessageDialog(null,"Código do produto: "+cod_produto);
    }
    void Comparar(String comp){
        if(comp == cod_produto){
            JOptionPane.showMessageDialog(null, "São produtos diferentes");
        }else{
            JOptionPane.showMessageDialog(null, "É o mesmo produto!"); 
        }
    }
}
